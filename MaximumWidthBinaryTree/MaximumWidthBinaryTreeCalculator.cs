﻿using BinaryTree;
using System.Collections.Generic;

namespace MaximumWidthBinaryTree
{
    class MaximumWidthBinaryTreeCalculator<T>
    {

        private static Queue<BinaryTreeNode<T>> _queue;

        public static int Calculate(BinaryTree<T> binaryTree)
        {

            if (binaryTree.Root == null)
                return 0;

            _queue = new Queue<BinaryTreeNode<T>>();
            _queue.Enqueue(binaryTree.Root);
            var maxWidth = 0;
            while (_queue.Count > 0)
            {
                var n = _queue.Count;
                if (n > maxWidth)
                    maxWidth = n;

                for (var i = 0; i < n; i++)
                {
                    EnqueeChildrens(_queue.Dequeue());
                }
            }

            return maxWidth;
        }

        private static void EnqueeChildrens(BinaryTreeNode<T> node)
        {
            if (node.Left != null)
                _queue.Enqueue(node.Left);

            if (node.Right != null)
                _queue.Enqueue(node.Right);
        }

    }
}
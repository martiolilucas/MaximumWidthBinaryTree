﻿using BinaryTree;
using System;

namespace MaximumWidthBinaryTree
{
    class Program
    {

        static void Main(string[] args)
        {

            BinaryTree<int> btree = new BinaryTree<int>();
            btree.Root = new BinaryTreeNode<int>(1);
            btree.Root.Left = new BinaryTreeNode<int>(2);
            btree.Root.Right = new BinaryTreeNode<int>(3);

            btree.Root.Left.Left = new BinaryTreeNode<int>(4);
            btree.Root.Left.Right = new BinaryTreeNode<int>(4);

            btree.Root.Right.Right = new BinaryTreeNode<int>(5);
            btree.Root.Right.Left = new BinaryTreeNode<int>(5);


            btree.Root.Left.Left.Right = new BinaryTreeNode<int>(6);
            btree.Root.Right.Right.Right = new BinaryTreeNode<int>(7);

            btree.Root.Right.Right.Right.Right = new BinaryTreeNode<int>(8);

            Console.WriteLine("The maximun width for this tree is {0}.", MaximumWidthBinaryTreeCalculator<int>.Calculate(btree));

        }


    }
}
